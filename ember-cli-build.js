/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var pickFiles = require('broccoli-static-compiler');

module.exports = function(defaults) {
    var app = new EmberApp(defaults, {
        svgstore: {
            files: {
                sourceDirs: 'app/styles/img',
                outputFile: '/assets/icons.svg'
            }
        },
        ready: function() {
            FastClick.attach(document.body);
        }

    });

    var fonts = pickFiles('app/styles/fonts', {
        srcDir: '/',
        destDir: '/assets/fonts'
    });

    // Use `app.import` to add additional libraries to the generated
    // output files.
    //
    // If you need to use different assets in different
    // environments, specify an object as the first parameter. That
    // object's keys should be the environment name and the values
    // should be the asset to use in that environment.
    //
    // If the library that you are including contains AMD or ES6
    // modules that you would like to import into your application
    // please specify an object with the list of modules as keys
    // along with the exports of each module as its value.
    app.import("bower_components/mobiscroll-date-fr/js/mobiscroll.core.js");
    app.import("bower_components/mobiscroll-date-fr/js/mobiscroll.frame.js");
    app.import("bower_components/mobiscroll-date-fr/js/mobiscroll.scroller.js");
    app.import("bower_components/mobiscroll-date-fr/js/mobiscroll.util.datetime.js");
    app.import("bower_components/mobiscroll-date-fr/js/mobiscroll.datetimebase.js");
    app.import("bower_components/mobiscroll-date-fr/js/mobiscroll.datetime.js");
    app.import("bower_components/mobiscroll-date-fr/js/i18n/mobiscroll.i18n.fr.js");
    app.import('bower_components/fastclick/lib/fastclick.js');

    return app.toTree([fonts]);
};
