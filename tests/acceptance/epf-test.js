import { test } from 'qunit';
import moduleForAcceptance from 'perinat-ember/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | epf');

test('calcul epf', function(assert) {
  visit('/weight');

  click('.bip');
  click('.val-7');
  click('.val-5');

  click('.pc');
  click('.val-2');
  click('.val-7');
  click('.val-6');

  click('.pa');
  click('.val-2');
  click('.val-3');
  click('.val-9');

  click('.lf');
  click('.val-5');
  click('.val-9');


  andThen(function() {
    assert.equal(findWithAssert('.result p').text().trim(), '1382 g');
  });
});