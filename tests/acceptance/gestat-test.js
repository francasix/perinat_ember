import { test } from 'qunit';
import moduleForAcceptance from 'perinat-ember/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | gestat');

test('calcul gestat is correc', function(assert) {
	visit('/gestat');

  //attention date in english in input
  fillIn('.exam-date', '01/01/2016');
  fillIn('.start-preg-date', '12/01/2015');

  andThen(function() {
  	assert.equal(findWithAssert('.result p').text().trim(), '6 semaines et 3 jours');
  });
});