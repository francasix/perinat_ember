import { pluralWord } from 'perinat-ember/helpers/plural-word';
import { module, test } from 'qunit';

module('Unit | Helper | plural word');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = pluralWord([42]);
  assert.ok(result);
});
