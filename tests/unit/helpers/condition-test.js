import { condition } from 'perinat-ember/helpers/condition';
import { module, test } from 'qunit';

module('Unit | Helper | condition');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = condition([42, '===', 42]);
  assert.ok(result);
});