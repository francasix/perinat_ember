# Perinat-ember

This README outlines the details of collaborating on this Ember application.
A short introduction of this app could easily go here.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://www.ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://www.ember-cli.com/)
* Development Browser Extensions
* [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
* [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

## help links
https://emberjs.jsbin.com/xuhavokubi/edit?html,js,output
https://blog.farrant.me/how-to-access-the-store-from-within-an-ember-component/

## resources
https://www.iconfinder.com/icons/296812/close_cross_delete_icon#size=128

## Cordova
* `npm install ember-cordova`
* `npm install ember-cli-cordova`

## ios
* `ember generate cordova-init com.perinat.ios`
*  Overwrite config/environment.js? `Yes`
*  Overwrite .gitignore? `No`
* `ember cordova:build`
* `ember cordova:open` (launch xcode)
* `ember cordova run --platform=ios` (emulate) 

## android
* download SDK
* `ember generate cordova-init com.perinat.android --platform=android`
* `ember cordova:build --platform=android`
* link the phone
* `ember cordova run --platform=android`

### branch / version
* master = main branch
* develop = advanced, new features
* noCordova = without cordova (from master)
* online = app online
