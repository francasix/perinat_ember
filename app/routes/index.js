import Ember from 'ember';

export default Ember.Route.extend({
	start: true,
	beforeModel: function() {
		if (this.start) {
			this.transitionTo('gestat');
			this.start = false;
		}
	}

});
