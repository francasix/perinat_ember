import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    sendResult: function(result) {
      //update result property
      this.get('controller').set('result', result);
    }
  },
  perinat: Ember.inject.service(),
  model() {
    return this.get('perinat').getWeightData();
  }
});