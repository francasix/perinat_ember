import Ember from 'ember';

var pregnantDaysInMs = null,
  lcc = null,
  lccMin = 6,
  lccMax = 85,
  echoDateInMs,
  twelveHours = 43200000,
  result;

export default Ember.Route.extend({
  actions: {
    sendResult: function(value) {
      //get : lcc = value
      lcc = value ? this.get('perinat').getDdgData().objectAt(0).set('value', value) : this.get('perinat').getDdgData().objectAt(0).get('value');

      //get pegnant & exam date
      pregnantDaysInMs = ((8.052 * Math.sqrt(lcc) + 23.73) - 14) * 86400000;
      echoDateInMs = +new Date(Ember.$('.echo-date').val()) + twelveHours;

      //update result property
      if (lcc > lccMin && lcc < lccMax) {
        result = this.get('converter').formatDateFr(new Date((echoDateInMs - pregnantDaysInMs)));
        this.get('controller').set('result', result);
      } else {
        this.get('controller').set('result', null);
      }

      //update gestat & ddg service datas
      this.get('perinat').getGestat().objectAt(0).set('startPregDate', (echoDateInMs - pregnantDaysInMs));

      this.get('perinat').getDdgData().objectAt(0).set('echoDate', echoDateInMs);
    }
  },
  init() {
    //set lcc value
    this.set('value', this.get('perinat').getDdgData().objectAt(0).value);
  },
  model() {
    return this.get('perinat').getDdgData();
  },
  converter: Ember.inject.service(),
  perinat: Ember.inject.service()

});
