import Ember from 'ember';
var newExamDate,
  newPregDate;

export default Ember.Route.extend({
  actions: {
    sendResult: function() {
      //get inputs's value on wheel change
      newExamDate = this.get('perinat').convertToMs(Ember.$('input.exam-date').val());
      newPregDate = this.get('perinat').convertToMs(Ember.$('input.start-preg-date').val());

      //set object gestat --> model updating
      this.model().objectAt(0).set('result', this.get('perinat').newResultGestat(newExamDate, newPregDate));
      this.model().objectAt(0).set('examDate', newExamDate);
      this.model().objectAt(0).set('startPregDate', newPregDate);
    },
  },
  afterModel: function() {
    var self = this;
    this.transitionTo('gestat').then(function() {
      Ember.run.schedule('afterRender', function() {
        self.send('sendResult');
      });
    });
  },
  model() {
    return this.get('perinat').getGestat();
  },
  perinat: Ember.inject.service(),
});
