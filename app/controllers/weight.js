import Ember from 'ember';

export default Ember.Controller.extend({
  result: null,
  resetFocusAllBlockValue(datas) {
    datas.forEach(function(obj) {
      obj.set('focus', false);
    });
  },
  actions: {
    resetAllBlockValue: function(model) {
      model.forEach(function(k) {
        k.set('value', null);
      });
    }
  }
});