export default function() {
    this.transition(
        this.fromRoute('gestat'),
        this.toRoute('weight'),
        this.use('toLeft'),
        this.reverse('toRight')
        );
    this.transition(
        this.fromRoute('gestat'),
        this.toRoute('ddg'),
        this.use('toUp'),
        this.reverse('toDown')
        );
    this.transition(
        this.fromRoute('gestat'),
        this.toRoute('index'),
        this.use('toRight'),
        this.reverse('toLeft')
        );
    this.transition(
        this.fromRoute('weight'),
        this.toRoute('index'),
        this.use('toLeft'),
        this.reverse('toRight')
        );
    this.transition(
        this.fromRoute('ddg'),
        this.toRoute('weight'),
        this.use('toLeft'),
        this.reverse('toUp')
        );
    this.transition(
        this.fromRoute('ddg'),
        this.toRoute('index'),
        this.use('toRight'),
        this.reverse('toLeft')
        );

}
