import Ember from 'ember';
var newDate,
year,
month,
day;

export default Ember.Service.extend({
	formatDateEn: function(date) {
		newDate = new Date(date);
		year = newDate.getFullYear();
		month = ('0' + (newDate.getMonth() + 1)).slice(-2);
		day = ('0' + newDate.getDate()).slice(-2);
		return `${month}/${day}/${year}`;
	},
	formatDateFr: function(date) {
		newDate = new Date(date);
		year = newDate.getFullYear();
		month = ('0' + (newDate.getMonth() + 1)).slice(-2);
		day = ('0' + newDate.getDate()).slice(-2);
		return `${day}/${month}/${year}`;
	}
});
