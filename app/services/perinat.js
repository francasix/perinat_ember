import Ember from 'ember';
import Gestat from 'perinat-ember/models/gestat';
import Weight from 'perinat-ember/models/weight';
import Ddg from 'perinat-ember/models/ddg';

const fourteenDays = 1209600000;
const fiftyWeek = 30240000000;
const arr = ['pa', 'lf', 'pc'];
let epf = null,
    value = null;

let gestats = [
    Gestat.create({
        id: '1',
        examDate: new Date(),
        startPregDate: new Date(),
        result: null
    })
];

let weightData = [
    Weight.create({
        id: 0,
        key: 'bip',
        value: null,
        placeholder: "BIP",
        focus: true
    }),
    Weight.create({
        id: 1,
        key: "pc",
        value: null,
        placeholder: "PC",
        focus: false
    }),
    Weight.create({
        id: 2,
        key: "pa",
        value: null,
        placeholder: "PA",
        focus: false
    }),
    Weight.create({
        id: 3,
        key: "lf",
        value: null,
        placeholder: "LF",
        focus: false
    })
];

let ddgData = [
    Ddg.create({
        key: '',
        value: null,
        echoDate: new Date(),
        placeholderLcc: "LCC",
        focus: true
    })
];

export default Ember.Service.extend({
    getGestat() {
        return gestats;
    },
    getWeightData() {
        return weightData;
    },
    getDdgData() {
        return ddgData;
    },
    newResultGestat(newExamDate, newPregDate) {
        let result = (newExamDate - newPregDate) + fourteenDays;

        if (result < 0 || result === fourteenDays || result > fiftyWeek) {
            return null;
        }
        return result;
    },
    convertToMs(date) {
        let newDate = new Date(date);
        return newDate.getTime();
    },
    getValue(key) {
        weightData.forEach(function(obj) {
            if (obj.key === key) {
                value = obj.value || 0;
            }
        });
        return value / 10;
    },
    resultEpf() {
        epf = parseInt(Math.pow(10, (1.326 - 0.00326 * this.getValue(arr[0]) * this.getValue(arr[1]) + (0.0107 * this.getValue(arr[2]) + 0.0438 * this.getValue(arr[0])) + 0.158 * this.getValue(arr[1]))));

        if (epf <= 21) {
            return null;
        }
        return epf;
    }
});
