import Ember from 'ember';

const listPages = ['gestat', 'weight'],
    { getOwner } = Ember;
var nextPage;

export default Ember.Service.extend({
    routing: Ember.inject.service('-routing'),
    currentPage: 1,

    nextPage: function(param, currentPage) {
        if ((currentPage + param) < 0) {
            return listPages.length - 1;
        } else if ((currentPage + param) > listPages.length - 1) {
            return 0;
        } else {
            return currentPage + param;
        }
    },

    swipePage: function(param) {
        this.set('currentPage', listPages.indexOf(getOwner(this).lookup('controller:application').currentPath));

        nextPage = this.get('nextPage')(param, this.get('currentPage'));
        this.get('routing').transitionTo(listPages[nextPage]);
    }
});
