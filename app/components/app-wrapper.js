import RecognizerMixin from 'ember-gestures/mixins/recognizers';
import Ember from 'ember';

export default Ember.Component.extend(RecognizerMixin, {
    recognizers: 'swipe',
    navigation: Ember.inject.service(),
    swipeLeft() {
        this.get('navigation').swipePage(-1);
    },
    swipeRight() {
        this.get('navigation').swipePage(1);
    }
});
