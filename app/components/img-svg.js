import Ember from 'ember';
import config from 'ember-get-config';
const { baseURL } = config;

export default Ember.Component.extend({
  tagName: '',
  baseUrl: baseURL
});