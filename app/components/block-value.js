import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',
  focused: false,
  change: Ember.observer('value', function() {
    //send result to route action
    this.get('sendResult')(this.get('perinat').resultEpf());
  }),
  actions: {
    resetValue: function(model) {
      model.set('value', null);
    },
    trueFocusedInput: function(model,focus) {
      if (!focus) {
        //all values focus to false
        this.get('targetObject').resetFocusAllBlockValue(this.get('perinat').getWeightData());
        //clicked value focus to true
        model.set('focus', true);
      }
    }
  },
  perinat: Ember.inject.service(),
});