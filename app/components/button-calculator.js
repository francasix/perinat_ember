import Ember from 'ember';
var model,
    currentValue,
    oldValue;

export default Ember.Component.extend({
    tagName: 'li',
    actions: {
        sendValue() {
            model = this.get('controller').get('model');
            currentValue = this.get('controller').get('value');

            model.forEach(function(key) {
                oldValue = (key.value === null) ? '' : (key.value).toString();

                if (key.focus === true) {
                    key.set('value', oldValue + (currentValue).toString());
                }
            });
        }
    },
    click(event) {
        //animation btn on clicked
        event.target.setAttribute('id', 'clicked');
        setTimeout(function() {
            event.target.setAttribute('id', '');
        }, 270);
    }
});
