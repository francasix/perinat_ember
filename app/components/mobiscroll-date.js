import Ember from 'ember';
var today;

export default Ember.Component.extend({
    tagName: 'input',
    startDate: '',
    attributeBindings: ['value'],
    //init plugin mobiscroll
    didInsertElement() {

        Ember.$('input.date').mobiscroll().date({
            cssClass: 'perinat-date',
            mode: 'scroller',
            display: 'inline',
            lang: 'fr',
            height: 22,
            rows: 3,
            dateFormat: 'mm/dd/yy',
            dateOrder: 'DD ddmmyy',
            buttons: [{
                text: '',
                cssClass: 'cross',
                handler: (event, inst) => {
                    //reset wheels
                    inst.setVal(this.get('converter').formatDateEn(new Date()));
                    //reset input
                    today = this.get('converter').formatDateEn(new Date());
                    event.target.offsetParent.parentNode.querySelector('input').value = today;
                    this.change();
                }
            }]
        });
    },
    currentGestat() {
        return this.get('perinat').getGestat().objectAt(0);
    },
    currentDdg() {
        return this.get('perinat').getDdgData().objectAt(0);
    },
    init() {
        this._super();
        //set value inputs
        if (this.class === 'date exam-date') {
            this.set('value', this.get('converter').formatDateEn(this.currentGestat().get('examDate')) || this.get('converter').formatDateEn(new Date()));

        } else if (this.class === 'date start-preg-date') {
            this.set('value', this.get('converter').formatDateEn(this.currentGestat().get('startPregDate')) || this.get('converter').formatDateEn(new Date()));

        } else if (this.class === 'date echo-date') {
            this.set('value', this.get('converter').formatDateEn(this.currentDdg().get('echoDate')) || this.get('converter').formatDateEn(new Date()));
        }
    },
    change: Ember.observer('value', function() {
        this.sendAction('sendResult');
    }),
    perinat: Ember.inject.service(),
    converter: Ember.inject.service(),

});
