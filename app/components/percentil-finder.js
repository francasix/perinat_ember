import Ember from 'ember';
import _ from 'lodash/lodash';

var listPercentils = ['p3', 'p10', 'p50', 'p90', 'p97'],
maxValue,
minValue,
weeksOfGestat,
res;

export default Ember.Component.extend({
	//object at the right weeksOfGestat
	datasPercentil: Ember.computed('value', function() {
    //transform date ms to week
    weeksOfGestat = this.fromMsToWeek(this.get('perinat').getGestat().objectAt(0).result);
    return this.get('percentils').getPercentilObj(this.get('key'), weeksOfGestat);
  }),
	percentilMin: Ember.computed('value', function() {

		if (this.get('value') === null) {
			return;
		}
		var self = this;

    //smallest value next to input value
    minValue = _.last(self.get('datasPercentil').filter(function(v) {
    	return parseFloat(v) < self.get('value');
    })) || null;
    //get match percentil
    res = (minValue !== null) ? listPercentils[_.indexOf(this.get('datasPercentil'), minValue)] : null;
    return res;
  }),
	percentilMax: Ember.computed('value', function() {

		if (this.get('value') === null) {
			return;
		}
		var self = this;

    //highest value next to input value
    maxValue = _.first(self.get('datasPercentil').filter(function(v) {
    	return parseFloat(v) > self.get('value');
    })) || null;
    //get match percentil
    res = (maxValue !== null) ? listPercentils[_.indexOf(this.get('datasPercentil'), maxValue)] : null;
    return res;
  }),

  fromMsToWeek: function(date) {
    return Math.round(date / (1000 * 60 * 60 * 24 * 7));
  },
  perinat: Ember.inject.service(),
  percentils: Ember.inject.service()
});
