import Ember from 'ember';


export function day(resultInMs) {
	var day = null;
	var week = resultInMs / (1000 * 60 * 60 * 24 * 7);
	var weekOneDecimal = Math.floor(week * 10) / 10;
	var rest = Math.round((weekOneDecimal - Math.floor(week)) * 10)/10;

	day = (rest < 0.9) ? Math.round((week - Math.floor(week)) * 7) : 0;

	return day;
}

export default Ember.Helper.helper(day);