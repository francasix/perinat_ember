import Ember from 'ember';

export function week(resultInMs) {
	var week = resultInMs / (1000 * 60 * 60 * 24 * 7);
	var weekOneDecimal = Math.floor(week * 10) / 10;
	var rest = Math.round((weekOneDecimal - Math.floor(week)) * 10)/10;

	week = (rest >= 0.9) ? Math.round(week) : parseInt(week);

	return week;
}

export default Ember.Helper.helper(week);