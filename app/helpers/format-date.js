import Ember from 'ember';

export function formatDate(date) {
	var newDate = new Date(date);
	var year = newDate.getFullYear();
	var month = ('0' + (newDate.getMonth()+1)).slice(-2);
	var day = ('0' + newDate.getDate()).slice(-2);
	return `${year}-${month}-${day}`;
}

export default Ember.Helper.helper(formatDate);
