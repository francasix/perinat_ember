import Ember from 'ember';

export function pluralWord(params) {
  if (params[2] > 1) {
    return params[1];
  } else {
    return params[0];
  }
}

export default Ember.Helper.helper(pluralWord);