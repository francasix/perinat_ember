import Ember from 'ember';

export function condition(params) {
  switch (params[1]) {
    case '!==':
      return (params[0] !== params[2]) ? true : false;
    case '===':
      return (params[0] === params[2]) ? true : false;
    case '<':
      return (params[0] < params[2]) ? true : false;
    case '<=':
      return (params[0] <= params[2]) ? true : false;
    case '>':
      return (params[0] > params[2]) ? true : false;
    case '>=':
      return (params[0] >= params[2]) ? true : false;
    case '&&':
      return (params[0] && params[2]) ? true : false;
    case '||':
      return (params[0] || params[2]) ? true : false;
    default:
      return false;
  }
}

export default Ember.Helper.helper(condition);