import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
    location: config.locationType
});

Router.map(function() {
  this.route('index', { path: '/' });

  this.route('gestat', function() {
      this.route('index', { path: '/' });
  });
  this.route('weight', function() {
      this.route('index', { path: '/' });
  });
  this.route('ddg', function() {
      this.route('index', { path: '/' });
  });
  this.route('info');
});

export default Router;
